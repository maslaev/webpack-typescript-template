const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin')

module.exports = {
   mode: 'development',
   context: path.resolve(__dirname, 'src'),
   entry: {
      main: './index.ts'
   },
   output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].[contenthash].js'
   },
   resolve: {
   extensions: ['.ts', '.tsx', '.js', '.jsx']
   },
   devtool: 'source-map',
   module: {
   rules: [
      {
         test: /\.tsx?$/,
         loader: 'awesome-typescript-loader'
      }
   ]
   },
   devServer: {
      port: 4201
   },
   plugins: [
      new HTMLWebpackPlugin({
         template: "./index.html"
      }),
      new CleanWebpackPlugin()
   ],
   optimization: {
      splitChunks: {
         chunks: 'all'
      }
   }
};