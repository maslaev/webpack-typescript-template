# Template

## Installation
#### 1. Make sure you have installed node.js version 12 or higher and npm
#### 2. Install all dependences from package.json with npm install
#### 3. Install some dev-dependences globally if you haven’t done it yet, with:
#### - npm install -g webpack webpack-cli webpack-dev-server typescript

## Compilation
#### Type **webpack** for compilation
#### Also you can try usable script, which runs app at port 4201 and auto reloads it:
#### - npm run devserver

## Starting
#### Open compiled index.html in /dist folder




